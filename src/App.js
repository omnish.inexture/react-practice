import { useRoutes } from "react-router-dom";
import NewComponent from "./components/hoc/NewComponent";
import Home from "./components/Home";
import Navbar from "./components/Navbar";
import ReduxDemo from "./components/redux/ReduxDemo";
import UseMemo from "./components/useMemo/UseMemo";
import CustomHook from "./hooks/CustomHook";
import store from "./components/redux/store";
import { Provider } from "react-redux";
import UseCallback from "./components/useCallback/UseCallback";

import "./app.css";

const Routes = () => {
    const route = useRoutes([
        { path: "/", element: <Home /> },
        { path: "/useMemo", element: <UseMemo /> },
        { path: "/useCallback", element: <UseCallback /> },
        { path: "/customHook", element: <CustomHook /> },
        { path: "/hoc", element: <NewComponent /> },
        { path: "/redux", element: <ReduxDemo /> },
    ]);

    return route;
};

function App() {
    return (
        <>
            <Provider store={store}>
                <Navbar />
                <Routes />
            </Provider>
        </>
    );
}

export default App;
