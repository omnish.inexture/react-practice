import React, { useMemo, useState } from "react";

const UseMemo = () => {
    const [firstCounter, setFirstCounter] = useState(0);
    const [secondCounter, setSecondCounter] = useState(0);

    // without usememo it take some time to update ui for both counter but we use even odd in only firstcounter
    // const isEven = () => {
    //     let i = 0;
    //     while (i < 200000000) i++;
    //     return firstCounter % 2 === 0;
    // };

    // with usememo it take some time to update ui for counter one , counter2's value is update without ui lag because first counter value is cached by the use memo and when cached value is not change than ui is rendering faster
    // const isEven = useMemo(() => {
    //     let i = 0;
    //     while (i < 200000000) i++;
    //     return firstCounter % 2 === 0;
    // }, [firstCounter]);

    return (
        <div className="useMemo container">
            <div>
                <button onClick={() => setFirstCounter(firstCounter + 1)}>
                    counter1
                    <span>{firstCounter}</span>
                </button>
                {/* for without useMemo */}
                {/* {isEven() ? <p>even</p> : <p>odd</p>} */}

                {/* for with useMemo */}
                {/* {isEven ? <p>even</p> : <p>odd</p>} */}
            </div>

            <div>
                <button onClick={() => setSecondCounter(secondCounter + 1)}>
                    counter2
                    <span>{secondCounter}</span>
                </button>
            </div>
        </div>
    );
};

export default UseMemo;
