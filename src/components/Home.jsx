import React from "react";
import { Link } from "react-router-dom";

const Home = () => {
    return (
        <div className="home container">
            <Link to="/useMemo">
                <div className="homeBox">UseMemo</div>
            </Link>
            <Link to="/useCallback">
                <div className="homeBox">UseCallback</div>
            </Link>
            <Link to="/customHook">
                <div className="homeBox">Custom Hook</div>
            </Link>
            <Link to="/hoc">
                <div className="homeBox">HOC</div>
            </Link>
            <Link to="/redux">
                <div className="homeBox">Redux</div>
            </Link>
        </div>
    );
};

export default Home;
