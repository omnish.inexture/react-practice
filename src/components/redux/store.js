import { configureStore } from "@reduxjs/toolkit";
import counterRecuer from "./slices/counterSlice";

const store = configureStore({ reducer: { counter: counterRecuer } });

export default store;
