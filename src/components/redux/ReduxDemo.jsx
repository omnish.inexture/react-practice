import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { increment, decrement } from "./slices/counterSlice";

const ReduxDemo = () => {
    const { value } = useSelector((state) => state.counter);
    const dispatch = useDispatch();
    return (
        <div className="redux container">
            <div className="redxcounterBox">
                <button
                    className="button1"
                    onClick={() => dispatch(decrement())}
                >
                    -
                </button>
                <h1>{value}</h1>
                <button
                    className="button2"
                    onClick={() => dispatch(increment())}
                >
                    +
                </button>
            </div>
        </div>
    );
};

export default ReduxDemo;
