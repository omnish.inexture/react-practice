import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
    return (
        <div className="navbar">
            <ul>
                <li>
                    <Link to="/useMemo">UseMemo</Link>
                </li>
                <li>
                    <Link to="/useCallback">UseCallback</Link>
                </li>
                <li>
                    <Link to="/customHook">CustomHook</Link>
                </li>
                <li>
                    <Link to="/hoc">HOC</Link>
                </li>
                <li>
                    <Link to="/redux">Redux</Link>
                </li>
            </ul>
        </div>
    );
};

export default Navbar;
