import React, { useCallback, useState } from "react";
import Button from "./Button";
import Count from "./Count";
import Title from "./Title";

const UseCallback = () => {
    const [age, setAge] = useState(22);
    const [salary, setSalary] = useState(50000);

    const incrementAge = useCallback(() => {
        setAge(age + 1);
    }, [age]);

    const incrementSalary = useCallback(() => {
        setSalary(salary + 1000);
    }, [salary]);

    return (
        <div className="container useCallback">
            <Title />
            <div className="box">
                <Count text="Age" count={age} />
                <Button handleClick={incrementAge}>Increment Age</Button>
            </div>

            <div className="box">
                <Count text="Salary" count={salary} />
                <Button handleClick={incrementSalary}>Increment Salary</Button>
            </div>
        </div>
    );
};

export default UseCallback;
