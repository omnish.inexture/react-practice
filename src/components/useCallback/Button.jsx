import React from "react";

const Button = ({ handleClick, children }) => {
    console.log(`Rendering button -`, children);
    return (
        <button className="useCallbackButton" onClick={handleClick}>
            {children}
        </button>
    );
};

export default React.memo(Button);
