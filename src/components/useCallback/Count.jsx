import React from "react";

const Count = ({ text, count }) => {
    console.log(`Rendering ${text}`);
    return (
        <div className="useCallbackCount">
            {text} - {count}
        </div>
    );
};

export default React.memo(Count);
