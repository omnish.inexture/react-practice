import React from "react";

const Title = () => {
    console.log("Rendering Ttitle Component");
    return <h1 className="useCallbackTitle">UseCallback function</h1>;
};

export default React.memo(Title);
