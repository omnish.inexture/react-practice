import React from "react";
import EnhancedComponent from "./EnhancedComponent";

const NewComponent = ({ name }) => {
    return (
        <div className="container hoc">
            <h1>{name}</h1>
        </div>
    );
};

export default EnhancedComponent(NewComponent);
