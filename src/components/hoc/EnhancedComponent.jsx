import React from "react";

const EnhancedComponent = (OrigionalComponent) => {
    class NewComponent extends React.Component {
        render() {
            return <OrigionalComponent name="John" />;
        }
    }
    return NewComponent;
};

export default EnhancedComponent;
