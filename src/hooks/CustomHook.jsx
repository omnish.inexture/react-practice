import React from "react";
import UseFetch from "./useFetch";

const CustomHook = () => {
    const [data] = UseFetch("https://jsonplaceholder.typicode.com/posts");

    return (
        <div className="container customHook">
            <div className="customHookSecondaryContainer">
                {data.slice(0, 20).map((e, i) => (
                    <div className="customHookBox" key={i}>
                        <h4>{e.title}</h4>
                        <p>{e.body}</p>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default CustomHook;
